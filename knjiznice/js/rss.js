var FEED_URL = " https://cors-anywhere.herokuapp.com/http://rss.medicalnewstoday.com/hypertension.xml";
var news;
var stevilo;

$(document).ready(function() {
    pridobiNovice();
});

function pridobiNovice() {
    $.ajax({
        url: FEED_URL,
        type: 'GET',
        success: function (data) {
            console.log("Requesting RSS feed successful!");
            news = parseXML(data);
            stevilo = news.length - 1;
            prikaziFeed(stevilo);
            // Call: title, pubDate, link, guid, description, category
        },
        error: function (err) {
            console.log("Pripetila se je napaka\n" + err);
        }
    });
}

function parseXML(obj) {
    var xml = $(obj).find('channel');
    var items = [];

    $(xml).find('item').each(function (index) {
        items[index] = [];
        $(this).children().each(function () {
            items[index][(this).nodeName] = $(this).text();
        })
    });
    return items;
}

function prikaziFeed(i) {
    $("#hints").html("<a target='_blank' href='" + news[i].link + "'>" + news[i].title + "</a><br>" + news[i].description);

    i = Math.floor((Math.random()*stevilo) + 1);
    //console.log(i)
    if (--i > -1) {
        setTimeout(function () { prikaziFeed(i); }, 9000);
    } else {
        i = stevilo;
        setTimeout(function () { prikaziFeed(i); }, 9000);
    }
}
