var info;
var diaY_sum = 0;
var sisY_sum = 0;
var arrlen;
var casX = [];
var diaY = [];
var sisY = [];
var povp_diaY;
var povp_sisY;

$(document).ready(function() {
    preberiMeritveVitalnihZnakov();
    // preberiMeritveVitalnihZnakov naredi:
        // graf
        // izriše obvestilo o stanju bolezni

    preberiEHRodBolnika();
});

/**
 * Funkcija v okno stanje bolezni izpiše v katerem stanju
 * je trenutno bolnik
 */
function stanjeBolezni(diaY, sisY, arrlen) {
    for (var h = 0; h < arrlen; h++) {
        diaY_sum += diaY[h];
        sisY_sum += sisY[h];
    }

    povp_diaY = diaY_sum/arrlen;
    povp_sisY = sisY_sum/arrlen;

    if (povp_sisY < 160 && povp_sisY >= 140 || povp_diaY <= 99 && povp_diaY >= 90) {
        // Blaga hipertenzija
        $("#obvestila").html(
            '<div class="alert alert-info" role="alert">' +
            '</button><strong>Pozor!</strong> Glede na zadnje meritve ' +
            'imate blago povišan krvni tlak.</div>'
        );

    } else if (povp_sisY < 180 && povp_sisY >= 160 || povp_diaY <= 109 && povp_diaY >= 100) {
        // Zmerna hipertenzija
        $("#obvestila").html(
            '<div class="alert alert-warning" role="alert">' +
            '</button><strong>Pozor!</strong> Glede na zadnje meritve ' +
            'imate zmerno povišan krvni tlak. Priporočamo obisk zdravnika.</div>'
        );

    } else if (povp_sisY >= 180  ||  povp_diaY >= 110) {
        // Huda hipertenzija
        $("#obvestila").html(
            '<div class="alert alert-danger" role="alert">' +
            '</button><strong>Pozor!</strong> Glede na zadnje meritve ' +
            'imate hudo povišan krvni tlak. Nujno obiščite zdravnika!</div>'
        );

    } else if (povp_sisY < 140  ||  povp_diaY < 90) {
        // Samo zdravje
        $("#obvestila").html(
            '<div class="alert alert-success" role="alert">' +
            '</button><strong>Super!</strong> Glede na zadnje meritve ' +
            'nimate povišanega krvnega tlaka.</div>'
        );
    }
    diaY_sum = 0;
    sisY_sum = 0;
}

/**
 * Funkcija za master detail 1
 */
function prikaziPodatkeOBolniku() {
    $("#stanjeBolnika").html("" +
            "<button type='button' class='btn btn-default btn-xs' onclick='skrijPodatkeOBolniku()'>" +
            "<strong><span class='glyphicon glyphicon-minus'></span>" +
            "<span class='naslov'>  Podatki o bolniku</strong></button><br>" +
            "<strong>" + info.firstNames + " " + info.lastNames + "</strong><br>" +
            "Rojstni dan: " + info.dateOfBirth.substring(0,10)  + "<br>" +
            "Povprečni krvni tlak: " + Math.floor(povp_sisY) + "/" + Math.floor(povp_diaY) + "<br>"
    );
}

/**
 * Funkcija za master detail 2
 */
function skrijPodatkeOBolniku() {
    $("#stanjeBolnika").html("" +
        "<button type='button' class='btn btn-default btn-xs' onclick='prikaziPodatkeOBolniku()'>" +
        "<strong><span class='glyphicon glyphicon-plus'></span>" +
        "<span class='naslov'>  Podatki o bolniku</strong></button>"
    );
}

/**
 * Za izbranega bolnika pridobi njegove podrobnosti in izpiši
 * v master/detail (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
    sessionId = getSessionId();

    var ehrId = $("#preberiObstojeciEHR").val();

    if (!ehrId || ehrId.trim().length == 0) {
        $("#statusZahteve").html("<span class='obvestilo label label-warning " +
            "fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                info = data.party;
            },
            error: function(err) {
                $("#statusZahteve").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}

/**
 * Funkcija omogoča oddajanje meritev na strežnik
 * ob tem še posodobi graf
 */
function dodajMeritveVitalnihZnakov() {
    sessionId = getSessionId();

    var ehrId = $("#preberiObstojeciEHR").val();
    var datumInUra = $("#dodajDatumCas").val();
    var telesnaVisina = 170;
    var telesnaTeza = 80;
    var telesnaTemperatura = 37;
    var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
    var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    var nasicenostKrviSKisikom = 98;
    var merilec = "Papa";

    if (sistolicniKrvniTlak === "" || diastolicniKrvniTlak === "" || datumInUra === "") {
        console.log("Ni podatkov");
        $("#statusZahteve").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        console.log("Podatki so");
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            // Struktura predloge je na voljo na naslednjem spletnem naslovu:
            // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
            "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
            "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
            "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
            committer: merilec
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + "Meritev dodana!</span>");
                console.log(res.meta.href)
            },
            error: function(err) {
                $("#statusZahteve").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
        preberiMeritveVitalnihZnakov();
    }
}

/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
/**
 * Funkcija iz APIja zahteva podatke in jih prikaže v grafu
 */
function preberiMeritveVitalnihZnakov() {
    sessionId = getSessionId();
    var ehrId = $("#preberiObstojeciEHR").val();

    if (!ehrId || ehrId.trim().length === 0) {
        $("#statusZahteve").html("<span class='obvestilo " +
            "label label-warning fade-in'>Vnešen ehrID ni veljaven");
    } else {
        $.ajax({
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (data) {
                var AQL =
                    "select \
                        t/data[at0001]/events[at0006]/time/value as cas, \
                        t/data[at0001]/events[at0006]/data[at0003]/items[at0004]/value/magnitude as tlak_s, \
                        t/data[at0001]/events[at0006]/data[at0003]/items[at0005]/value/magnitude as tlak_d \
                        from EHR e[e/ehr_id/value='" + ehrId + "'] \
                        contains OBSERVATION t[openEHR-EHR-OBSERVATION.blood_pressure.v1] \
                        order by t/data[at0001]/events[at0006]/time/value desc \
                        limit 31";
                $.ajax({
                    url: baseUrl + "/query?" + $.param({"aql": AQL}),
                    type: 'GET',
                    headers: {"Ehr-Session": sessionId},
                    success: function (res) {
                        if (res) {
                            $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" +
                                "Poizvedba uspela!</span>");

                            arrlen = res.resultSet.length;
                            casX = [];
                            diaY = [];
                            sisY = [];

                            for (var f = 0; f < arrlen; f++) {
                                casX[f] = 30 - f;
                                diaY[f] = res.resultSet[f].tlak_d;
                                sisY[f] = res.resultSet[f].tlak_s;
                            }

                            dia = {
                                x: casX,
                                y: diaY,
                                type: 'lines+markers',
                                name: 'Diastola'
                            };

                            sis = {
                                x: casX,
                                y: sisY,
                                type: 'lines+markers',
                                name: 'Sistola'
                            };

                            var data = [dia, sis];

                            var layout = {
                                autosize: true,
                                height: 230,
                                margin: {
                                    l: 50,
                                    r: 0,
                                    b: 50,
                                    t: 0,
                                    pad: 4
                                },
                                yaxis: {
                                    range: [0, 200],
                                    autorange: false
                                },
                                xaxis: {
                                    range: [0, 31],
                                    autorange: false
                                }
                            };

                            $("#graph").html("");
                            Plotly.newPlot('graph', data, layout);

                            preberiEHRodBolnika();
                            stanjeBolezni(diaY, sisY, arrlen);
                            skrijPodatkeOBolniku();

                        } else {
                            $("#statusZahteve").html(
                                "<span class='obvestilo label label-warning fade-in'>" +
                                "Ni podatkov!</span>");
                        }

                    },
                    error: function (err) {
                        $("#statusZahteve").html(
                            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                            JSON.parse(err.responseText).userMessage + "'!");
                    }
                });
            },
            error: function (err) {
                $("#statusZahteve").html(
                    "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}