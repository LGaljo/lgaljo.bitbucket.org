var bolnik_prvi_EhrId = "ec0beb93-cf8f-4526-b04d-71464bb9d09d";
var bolnik_drugi_EhrId = "1eda3bd7-47c6-4212-b785-a211b21a4760";
var bolnik_tretji_EhrId = "a176fb5a-9b2c-40d3-b6da-66a15bff4b27";

var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/*
* Funkcija generiranjePodatkov obnovi podatke, če so bili izbrisani
* pred generiranjem je nujno, da so bolniki kreirani in imajo svoj
* EhrId. Uporabnik je opozorjen, če tega še ni.
 */
function generiranjePodatkov() {
    if (bolnik_prvi_EhrId === null || bolnik_drugi_EhrId === null || bolnik_tretji_EhrId === null) {
        alert("Prosim najprej kreiraj bolnike!");
    } else {
        for (var i = 0; i < 31; i++) {
            generirajPodatke(bolnik_prvi_EhrId,
                {
                    "datumInUra": "2017-5-1T17:19:5Z",
                    "telesnaTemp": gen_temp(25),
                    "telesnaTeza": 24,
                    "telesnaVisina": 163,
                    "sisTlak": gen_tlak_sis(110),
                    "diasTlak": gen_tlak_dia(70),
                    "nasicSKis": gen_O2(97),
                    "merilec": "Mike"
                },
                {
                    "zdravnik": "Dr. med. Simon Stopar",
                    "zdUstanova": "Zdravstveni dom Ljubljana",
                    "navodila": "Jemlji na 2 krat na dan",
                    "casInt": 4,
                    "kolicinaDnevno": 2,
                    "zac": "1984-4-29T17:19:5Z",
                    "kon": "1990-12-21T19:18:53.526Z",
                    "smernice": "Pijte veliko tekocine",
                    "zdravilo": "Amoxil"
                }
            );

            generirajPodatke(bolnik_drugi_EhrId,
                {
                    "datumInUra": "2017-5-1T17:19:5Z",
                    "telesnaTemp": gen_temp(36),
                    "telesnaTeza": 65,
                    "telesnaVisina": 140,
                    "sisTlak": gen_tlak_sis(140),
                    "diasTlak": gen_tlak_dia(100),
                    "nasicSKis": gen_O2(90),
                    "merilec": "Micka"
                },
                {
                    "zdravnik": "Dr. med. Nives Avancini",
                    "zdUstanova": "Zdravstveni dom Grosuplje",
                    "navodila": "Jemlji na 2 krat na dan",
                    "casInt": 4,
                    "kolicinaDnevno": 2,
                    "zac": "1984-4-29T17:19:5Z",
                    "kon": "1990-12-21T19:18:53.526Z",
                    "smernice": "Pijte veliko tekocine",
                    "zdravilo": "Aspirin"
                }
            );

            generirajPodatke(bolnik_tretji_EhrId,
                {
                    "datumInUra": "2017-5-1T17:19:5Z",
                    "telesnaTemp": gen_temp(37),
                    "telesnaTeza": 102,
                    "telesnaVisina": 176,
                    "sisTlak": gen_tlak_sis(160),
                    "diasTlak": gen_tlak_dia(120),
                    "nasicSKis": gen_O2(97),
                    "merilec": "Boom"
                },
                {
                    "zdravnik": "Dr. Peter Zorman",
                    "zdUstanova": "Zdravstveni dom Novo Mesto",
                    "navodila": "Jemlji na 2 krat na dan",
                    "casInt": 4,
                    "kolicinaDnevno": 4,
                    "zac": "2001-4-29T17:19:5Z",
                    "kon": "2003-12-21T19:18:53.526Z",
                    "smernice": "Pred uporabo bodite tesci",
                    "zdravilo": "Omnicef"
                }
            );
        }
    }
}

/*
* Funkcija generirajPodatke glede na tri parametre s POST zahtevo pošlje podatke na API
* @param ehrId - identifikacijska številka
* @param podatkiVS - podatki o bolniku {datumInUra, telesnaTemp, telesnaTeza, telesnaVisina, sisTlak, diasTlak, nasicSKis, merilec, }
* @param podatkiMed - podatki o zdravniku {zdravnik, zdUstanova, navodila, casInt, kolicinaDnevno, zac, kon, smernice, zdravilo}
 */
function generirajPodatke(ehrId, podatkiVS) {

    var sessionId = getSessionId();

    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });

    var podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": podatkiVS["datumInUra"],
        "vital_signs/height_length/any_event/body_height_length": podatkiVS["telesnaVisina"],
        "vital_signs/body_weight/any_event/body_weight": podatkiVS["telesnaTeza"],
        "vital_signs/body_temperature/any_event/temperature|magnitude": podatkiVS["telesnaTemp"],
        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
        "vital_signs/blood_pressure/any_event/systolic": podatkiVS["sisTlak"],
        "vital_signs/blood_pressure/any_event/diastolic": podatkiVS["diasTlak"],
        "vital_signs/indirect_oximetry:0/spo2|numerator": podatkiVS["nasicSKis"]
    };

    var parametriZahteve = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: podatkiVS["merilec"]
    };

    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            console.log(res.meta.href);
        },
        error: function(err) {
            console.log(JSON.parse(err.responseText).userMessage);
        }
    });
    
    preberiMeritveVitalnihZnakov();
    $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + "Generirani!</span>");

}

/*
* Funkcija kreirajEhrZaBolnike na API pošlje zahtevo za ustvarjanje
* bolnika. V spremenljivke bolnik_prvi_EhrId, bolnik_drugi_EhrId,
* bolnik_tretji_EhrId zapiše njihove nove ehr IDje
*/
function kreirajEhrZaBolnike() {

    sessionId = getSessionId();

    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });

    // bolnik 1
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Pepe The Frog",
                lastNames: "#DeadMemes",
                dateOfBirth: "1972-7-18T19:20",
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action === 'CREATE') {
                        bolnik_prvi_EhrId = ehrId;
                        $("#prviBolnik").val(bolnik_prvi_EhrId);
                        console.log("Prvi bolnik dobil ID");
                        console.log(bolnik_prvi_EhrId);
                    }
                },
                error: function(err) {
                    console.log("Napaka!!! EhrId ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    // Bolnik 2
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "The Turkish Cook",
                lastNames: "#SaltBae",
                dateOfBirth: "2010-4-11T19:11",
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action === 'CREATE') {
                        bolnik_drugi_EhrId = ehrId;
                        $("#drugiBolnik").val(bolnik_drugi_EhrId);
                        console.log("Drugi bolnik dobil ID");
                        console.log(bolnik_drugi_EhrId);
                    }
                },
                error: function(err) {
                    console.log("Napaka!!! EhrId ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    // Bolnik 3
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',

        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: "Achmed",
                lastNames: "The Dead Terrorist",
                dateOfBirth: "1982-7-12T19:10",
                partyAdditionalInfo: [{key: "ehrId", value: ehrId},]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action === 'CREATE') {
                        bolnik_tretji_EhrId = ehrId;
                        $("#tretjiBolnik").val(bolnik_tretji_EhrId);
                        console.log("Tretji bolnik dobil ID");
                        console.log(bolnik_tretji_EhrId);
                    }
                },
                error: function(err) {
                    console.log("Napaka!!! EhrId ni bilo mogoče ustvariti! " + JSON.parse(err.responseText).userMessage);
                }
            });
        }
    });

    $("#statusZahteve").html("<span class='obvestilo label label-success fade-in'>" + "Bolniki ustvarjeni! (IDji v konzoli)</span>");
}

/*
 * Komplet funkcij, ki vračajo naključne podatke za ustvarjanje
 * raznolikih podatkov
 */
function gen_tlak_sis(base) {
    return base + Math.floor(Math.random()*20+1);
}

function gen_tlak_dia(base) {
    return base + Math.floor(Math.random()*20+1);
}

function gen_temp(base) {
    return base + Math.floor(Math.random()*2+1);
}

function gen_O2(base) {
    return base + Math.floor(Math.random()*2+1);
}